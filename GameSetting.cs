﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TroChoiDaoVangException
{
    public class GameSetting
    {
        public int GoldDigger(int goldInput)
        {
            if (goldInput <= 0)
            {
                throw new ArgumentException("Enter your input higher than 0");
            }

            int firstGold = Math.Min(goldInput, (int)GoldEnum.SO_QUANG_VANG_THU_NHAT);
            int price10Gold = firstGold * (int)GoldEnum.GIA_QUANG_VANG_THU_NHAT;
            goldInput -= firstGold;
            Console.WriteLine($"Nguoi choi nhan duoc {firstGold} tien dau tien.");

            int secondGold = Math.Min(goldInput, (int)GoldEnum.SO_QUANG_VANG_THU_HAI);
            int price5Gold = secondGold * (int)GoldEnum.GIA_QUANG_VANG_THU_HAI;
            goldInput -= secondGold;
            Console.WriteLine($"Nguoi choi nhan duoc {secondGold} tien thu hai.");

            int thirdGold = Math.Min(goldInput, (int)GoldEnum.SO_QUANG_VANG_THU_BA);
            int price3Gold = thirdGold * (int)GoldEnum.GIA_QUANG_VANG_THU_BA;
            goldInput -= thirdGold;
            Console.WriteLine($"Nguoi choi nhan duoc {thirdGold} tien thu ba.");

            int priceOfLastGold = goldInput * (int)GoldEnum.GIA_QUANG_VANG_CON_LAI;
            Console.WriteLine($"Nguoi choi nhan duoc {goldInput} tien con lai.");

            int totalOfPrice = (price10Gold + price5Gold + price3Gold + priceOfLastGold);
            return totalOfPrice;
        }

        public void Menu()
        {
            Console.WriteLine("---Keo Bua bao nang cao----");
            Console.WriteLine("1. Play");
            Console.WriteLine("2. Exit");
            Console.WriteLine("---------------------------");
            Console.WriteLine("Nhap lua chon: ");
        }
    }
}
