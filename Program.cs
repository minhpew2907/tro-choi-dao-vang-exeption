﻿using TroChoiDaoVangException;

GameSetting gameSetting = new GameSetting();
int choice = 0;
do
{
    try
    {
        gameSetting.Menu();
        choice = int.Parse(Console.ReadLine());
        Console.Clear();

        switch (choice)
        {
            case 1:
                Console.WriteLine("Nhap so vang ban nhan duoc: ");
                int goldInput = int.Parse(Console.ReadLine());

                int moneyReceive = gameSetting.GoldDigger(goldInput);
                Console.WriteLine($"Ban da nhan duoc {moneyReceive} tien");
                break;
            case 2:
                Console.WriteLine("Hen gap lai");
                Environment.Exit(0);
                break;
            default:
                Console.WriteLine("Ban nhap so bang tren.");
                break;
        }
    }
    catch (Exception ex)
    {
        Console.WriteLine(ex.Message);
    }
} while (choice != 2);