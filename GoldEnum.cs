﻿

namespace TroChoiDaoVangException
{
    public enum GoldEnum
    {
        SO_QUANG_VANG_THU_NHAT = 10,
        GIA_QUANG_VANG_THU_NHAT = 10,
        SO_QUANG_VANG_THU_HAI = 5,
        GIA_QUANG_VANG_THU_HAI = 5,
        SO_QUANG_VANG_THU_BA = 3,
        GIA_QUANG_VANG_THU_BA = 2,
        GIA_QUANG_VANG_CON_LAI = 1
    }
}
